// If not running in production mode - load all environment variables
if(process.env.NODE_ENV !== 'production'){
    require('dotenv').config()
}

const express = require('express')

const app = express()

const expressLayouts = require('express-ejs-layouts')

// router/controllers/REST methods
const indexRouter = require('./routes/index')

app.set('view engine', 'ejs')

// Declare location of views
app.set('views', __dirname + '/views')

//Location of header/footer
app.set('layout', 'layouts/layout')
app.use(expressLayouts)

// styles sheets, js, images/ public = convention
app.use(express.static('public'))

// Setup DB
const mongoose = require('mongoose')
mongoose.set('useUnifiedTopology', true)
mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true
})

const db = mongoose.connection
db.on('error', error => console.log(error))
db.once('open', () => console.log('Connected to db'))



// controller methods are delared in thsi
app.use('/', indexRouter)

// PORT = deployed/production - port will be assigned from server, 3000 = local development
app.listen(process.env.PORT || 3000)