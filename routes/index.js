const express = require('express');

// Get access to router
const router = express.Router()

router.get('/', (req, res) => {
    res.render('index')
})

// import this so that server.js can refer to it for the home page app.use('/')
module.exports = router